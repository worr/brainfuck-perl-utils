package Brainfuck::DataStore;

use Carp;

use strict;
use warnings;

use Brainfuck::DSNode;

sub new {
    my ($class) = @_;
    my $self = {};
    $self->{CUR} = undef;
    $self->{FIRST} = undef;
    $self->{LAST} = undef;
    $self->{COUNT} = 0;

    bless $self, $class;
    $self->first(0);

    return $self;
}

sub first {
    my ($self, $data);
    if (@_ == 1) {
        $self = shift;
        return $self->{FIRST};
    } elsif (@_ == 2) {
        ($self, $data) = @_;
    } else {
        confess "prepend requires 0 or 1 arguments";
    }

    if ($self->{COUNT} == 0) {
        $self->{FIRST} = Brainfuck::DSNode->new($data, undef, undef);
        $self->{CUR} = $self->{FIRST};
        $self->{LAST} = $self->{CUR};
    } else {
        $self->{FIRST} = Brainfuck::DSNode->new($data, undef, $self->{FIRST});
        $self->{FIRST}->next->prev($self->{FIRST});
    }

    $self->{COUNT} += 1;
}

sub last {
    my ($self, $data);

    if (@_ == 1) {
        $self = shift;
        return $self->{LAST};
    } elsif (@_ == 2) {
        ($self, $data) = @_;
    } else {
        confess "append requires 0-2 arguments";
    }

    if ($self->{COUNT} == 0) {
        $self->{LAST} = Brainfuck::DSNode->new($data, undef, undef);
        $self->{CUR} = $self->{LAST};
        $self->{FIRST} = $self->{CUR};
    } else {
        $self->{LAST} = Brainfuck::DSNode->new($data, $self->{LAST}, undef);
        $self->{LAST}->prev->next($self->{LAST});
    }

    $self->{COUNT} += 1;
}

sub count {
    my $self = shift;
    return $self->{COUNT};
}

sub cur {
    my $self = shift;
    if (@_) { $self->{CUR} = shift; }
    return $self->{CUR};
}

1;
