package Brainfuck::Text2Brainfuck;

use strict;
use warnings;
use integer;

use Carp;

use Brainfuck::DataStore;
use Brainfuck::Interpreter;

sub new {
    my $class = shift;
    my $message;

    if (@_) {
        $message = shift;
    }

    my $self = {};
    $self->{MESSAGE} = $message;
    $self->{CODE} = "";
    $self->{CHARACTERS} = undef;
    $self->{BASE_VALUES} = undef;
    $self->{BF_DATASTORE} = undef;

    return bless $self, $class;
}

sub message {
    my $self = shift;
    if (@_) { $self->{MESSAGE} = shift; }
    return $self->{MESSAGE};
}

sub code {
    my $self = shift;
    if (@_) { $self->{CODE} = shift; }
    return $self->{CODE};
}

sub _analyze {
    my $self = shift;
    my (@split_code, @base_values, @characters);
    @split_code = split //, $self->message;
    for (my $i = 0; $i < @split_code; $i += 1) {
        my $ord_character = ord $split_code[$i];
        my $tens = $ord_character / 10;
        push @characters, [ $ord_character, $tens ];

        push @base_values, $tens if (not grep { $_ == $tens } @base_values);
    }

    $self->{BASE_VALUES} = \@base_values;
    $self->{CHARACTERS} = \@characters;
}

sub _make_prep {
    my $self = shift;
    my $code = $self->code;
    my $base_values = $self->{BASE_VALUES};
    my $characters = $self->{CHARACTERS};
    my $bf;

    for (my $i = 0; $i < @$base_values; $i += 1) {
        $code = $code . "+" x 10 . "[" . ">" x ($i + 1) . "+" x $base_values->[$i] . "<" x ($i + 1) . "-]";
    }

    $self->code($code);
}

sub _make_code {
    my $self = shift;
    my $code = $self->code;
    my $characters = $self->{CHARACTERS};
    my $base_values = $self->{BASE_VALUES};
    my $base_pointer_location = 0;
    my $bf = Brainfuck::Interpreter->new($code);

    # Run the current code and prep the datastore
    $bf->run;

    my $datastore = $bf->datastore;

    foreach my $char (@$characters) {
        my $ord_char = $char->[0];
        my $tens = $char->[1];
        my $index = 0; 
        my $direction = ">";
        my $opp_direction = "<";
        my $switched = 0;
        my $char_bf_code;
        
        $index += 1 until ($base_values->[$index] == $tens or $index == @$base_values);

        $index += 1;

        # Switch values (this is cool)
        if ($base_pointer_location > $index) {
            $switched = 1;
            $direction ^= $opp_direction ^= $direction ^= $opp_direction;
        }

        $char_bf_code = $direction x abs($index - $base_pointer_location);
        $bf->code($char_bf_code);
        $bf->run;
        $code = $code . $char_bf_code;

        if ($datastore->cur->data < $char->[0]) {
            $char_bf_code = '+' x ($char->[0] - $datastore->cur->data);
        } else {
            $char_bf_code = '-' x ($datastore->cur->data - $char->[0]);

        }

        if ($switched == 1) {
            $direction ^= $opp_direction ^= $direction ^= $opp_direction;
        }

        $base_pointer_location = $index;

        $bf->code($char_bf_code);
        $bf->run;

        $code = $code . $char_bf_code . ".";
    }

    $self->code($code);
}

sub generate {
    my $self = shift;
    $self->_analyze;
    $self->_make_prep;
    $self->_make_code;

    return $self->code;
}

1;
