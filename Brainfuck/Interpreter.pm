package Brainfuck::Interpreter;

use strict;
use warnings;

use Carp;
use Switch;

use Brainfuck::DataStore;

sub new {
    my ($class, $code, $bf_data);
    if (@_ == 2) {
        ($class, $code) = @_;
        $bf_data = Brainfuck::DataStore->new;
    } elsif (@_ == 3) {
        ($class, $code, $bf_data) = @_;
    } else {
        confess "Brainfuck::Interpreter takes 1-2 arguments";
    }

    my $self = {};
    $self->{CODE} = $code;
    $self->{DATASTORE} = $bf_data;
    
    return bless $self, $class;
}

sub datastore {
    my $self = shift;
    if (@_) { $self->{DATASTORE} = shift; }
    return $self->{DATASTORE};
}

sub code {
    my $self = shift;
    if (@_) { $self->{CODE} = shift; }
    return $self->{CODE};
}

sub bf_print {
    my $self = shift;

    print chr $self->datastore->cur->data;
}

sub bf_input {
    my $self = shift;

    $self->datastore->cur->data(ord getc);
}

sub bf_increment {
    my $self = shift;

    $self->datastore->cur->inc;
}

sub bf_decrement {
    my $self = shift;

    $self->datastore->cur->dec;
}

sub bf_shift_left {
    my $self = shift;

    if (not defined $self->datastore->cur->prev) {
        $self->datastore->first(0);
    }

    $self->datastore->cur($self->datastore->cur->prev);
}

sub bf_shift_right {
    my $self = shift;

    if (not defined $self->datastore->cur->next) {
        $self->datastore->last(0);
    }

    $self->datastore->cur($self->datastore->cur->next);
}

sub run {
    my $self = shift;
    my $code = $self->code;
    my $datastore = $self->datastore;
    my $length = length $code;

    for (my $i = 0; $i < $length; $i += 1) {
        my $oper = substr $code, $i, 1;

        switch ($oper) {
            case '+' { $self->bf_increment; }
            case '-' { $self->bf_decrement; }
            case '>' { $self->bf_shift_right; }
            case '<' { $self->bf_shift_left; }
            case '.' { $self->bf_print; }
            case ',' { $self->bf_input; }
            case '[' { 
                my $parens = 1;
                my $j;
                for ($j = $i + 1; $j < $length; $j += 1) {
                    my $test_char = substr $code, $j, 1;

                    if ($test_char eq ']') {
                        $parens -= 1;
                    } elsif ($test_char eq '[') {
                        $parens += 1;
                    }

                    last if ($parens == 0);
                }

                my $loop_code = substr $code, $i + 1, $j - $i;
                substr($code, $i, $j - $i + 1) = "";
                
                print "$loop_code\n";
                print "$code\n";

                $i -= 1;
                $length = length $code;

                my $loop_obj = Brainfuck::Interpreter->new($loop_code, $datastore);
                while ($self->datastore->cur->data != 0) {
                    $loop_obj->run;
                }
            }
        }
    }
}

sub cleanup {
    my $self = shift;

    my @split_code = split / */, $self->code;
    @split_code = grep { !/[^\[\]\+\-\.\,\<\>]/ } @split_code;

    $self->code(join '', @split_code);
}

1;
