package Brainfuck::DSNode;

use strict;
use warnings;

use Carp;

sub new {
    my ($class, $data, $prev, $next) = @_;

    my $self = {};
    $self->{NEXT} = $next;
    $self->{PREV} = $prev;
    $self->{DATA} = $data;

    return bless $self, $class;
}

sub inc {
    my $self = shift;
    $self->{DATA} += 1;
}

sub dec {
    my $self = shift;
    $self->{DATA} -= 1;
}

sub next {
    my $self = shift;
    if (@_) { $self->{NEXT} = shift; }
    return $self->{NEXT};
}

sub prev {
    my $self = shift;
    if (@_) { $self->{PREV} = shift; }
    return $self->{PREV};
}

sub data {
    my $self = shift;
    if (@_) { $self->{DATA} = shift; }
    return $self->{DATA};
}

1;
