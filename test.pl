#!/usr/bin/perl

use strict;
use warnings;

use Brainfuck::Interpreter;

my $code = "++[>++[>++<-]<-]>>+++++++++++++++++++++++++++++++++++++++++++++++++++++++++.";
my $bf = Brainfuck::Interpreter->new($code);
$bf->run;
