#!/usr/bin/perl 

use strict;
use warnings;

use Brainfuck::Interpreter;
use Brainfuck::Text2Brainfuck;

my $message = "Hello World!\n";
my $bf_gen = Brainfuck::Text2Brainfuck->new($message);

my $code = $bf_gen->generate;

my $bf = Brainfuck::Interpreter->new($code);
$bf->run;

# Hello World in 410 lines!
